module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    {
        name      : 'Auth API Test',
        script    : './build/auth.js',
        watch: false,
        env: {
          "NODE_ENV": "testing",
        }
    },
    {
        name      : 'Offices API Test',
        script    : './build/offices.js',
        watch: false,
        env: {
            "NODE_ENV": "testing",
        }
    },
    {
        name      : 'Record API Test',
        script    : './build/record.js',
        watch: false,
        env: {
            "NODE_ENV": "testing",
        }
    },
    {
        name      : 'Common API Test',
        script    : './build/common.js',
        watch: false,
        env: {
            "NODE_ENV": "testing",
        }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : 'root',
      host : '179.60.149.123',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:snikius/sb_rec.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.dev.config.js'
    },
  }
};
