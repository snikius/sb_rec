// @flow
const {TRecord, TOffice} = require('./../models');
const {timeToDateStr, timeToStr} = require('./common');

function createRecord(data: TRecord, sqlClient: any, clientRedis: any): Promise<TRecord & TOffice> {
    const query = {
        text: 'INSERT INTO records(record, user_id, office_id, user_name) VALUES ($1, $2, $3, $4) RETURNING *',
        values: [new Date(data.record), data.user_id, data.office_id, data.user_name],
    };
    return checkDayRecord(data.user_id, data.record, data.office_id, sqlClient).then(() => sqlClient.query(query)).then((res) => {
        const office_id = res.rows[0].office_id;
        const date = new Date(res.rows[0].record);
        const dayStr = timeToDateStr(date);
        clientRedis.hset(`records:${office_id}:${dayStr}`, timeToStr(date), data.user_id);
        return getRecord(res.rows[0].id, sqlClient);
    });
}

function checkDayRecord(user_id: string, record: string, office_id: string, sqlClient: any): Promise<void> {
    const query = {
        text:
        'SELECT records.id ' +
        'FROM records ' +
        'WHERE records.user_id = $1 AND DATE(record) = DATE($2) AND office_id = $3',
        values: [user_id, new Date(record), office_id],
    };
    return new Promise((resolve, reject) => {
        sqlClient.query(query).then((res) => {
            !res.rows.length ? resolve() : reject("У Вас уже есть запись на выбранный день. Если Вам необходимо перезаписаться, пожалуйста, удалите запись в разделе \"Мои записи в отделение\".");
        });
    });
}

function getRecord(record_id: string, sqlClient: any): Promise<TRecord & TOffice> {
    const query = {
        text:
        'SELECT offices.name, offices.coords, offices.schedule, records.id, records.office_id, offices.address, records.record, records.created, records.user_id, records.user_name ' +
        'FROM records ' +
        'INNER JOIN offices ON offices.id = records.office_id ' +
        'WHERE records.id = $1 ',
        values: [record_id],
    };
    return sqlClient.query(query).then((res) => {
        return res.rows.length ? res.rows[0] : {};
    });
}

function getRecords(user_id: string, sqlClient: any): Promise<Array<TRecord & TOffice>> {
    const query = {
        text:
            'SELECT ' +
                'offices.name, ' +
                'offices.coords, ' +
                'offices.schedule, ' +
                'records.id, ' +
                'records.office_id, ' +
                'records.record, ' +
                'records.created, ' +
                'records.user_id, ' +
                'offices.address, ' +
                '(records.record > NOW()) AS is_active ' +
            'FROM records ' +
            'INNER JOIN offices ON offices.id = records.office_id ' +
            'WHERE records.user_id = $1 ' +
            'ORDER BY records.record DESC',
        values: [user_id],
    };
    return sqlClient.query(query).then((res) => {
        return res.rows.length ? res.rows : [];
    });
}

function deleteRecord(user_id: string, record_id: string, sqlClient: any, clientRedis: any): Promise<void> {
    const query = {
        text:
        'DELETE FROM records ' +
        'WHERE user_id = $1 AND id = $2 RETURNING *',
        values: [user_id, record_id],
    };
    return sqlClient.query(query).then((res) => {
        if (res.rows[0]) {
            const office_id = res.rows[0].office_id;
            const date = new Date(res.rows[0].record);
            const dayStr = timeToDateStr(date);
            clientRedis.hdel(`records:${office_id}:${dayStr}`, timeToStr(date));
        }
        return true;
    }, (err) => {
        return err.stack;
    });
}

module.exports = {createRecord, getRecords, deleteRecord};