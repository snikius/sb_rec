// @flow
const nodemailer = require('nodemailer');
const TelegramBot = require('node-telegram-bot-api');

const token = '413641414:AAGZ6vdXCcog-K8pF-tJEYChFbBozu_ZE0Y';
const chatId = '-182130592';  // -182130592 // -270327608
const bot = new TelegramBot(token);

type redisFunctionsType = 'set' | 'get' | 'del' | 'hkeys' | 'hset';

function addVersion(data: any, version: string) {
    return Object.assign(data, {version});
}

function toPromiseWrapper(client: any) {
    return (f: redisFunctionsType) => {
        if (f === 'set') {
            return (key: string, value: string, param: string | null = null, param_value: string | null = null) => {
                client[f](key, value, param, param_value);
            }
        }
        if (f === 'hset') {
            return (key: string, field: string, value: string) => {
                client[f](key, field, value);
            }
        }
        return (key: string) => {
            return new Promise((resolve) => {
                client[f](key, (result, res) => { resolve(res); });
            });
        }
    };
}

function timeToStr(date: Date): string {
    return date.toISOString();
}

function timeToDateStr(date: Date, separator: string = ''): string {
    const monthNumber = date.getMonth() + 1;
    const day = date.getDay() < 10 ? "0" + date.getDay() : date.getDay();
    return "" + date.getFullYear() + separator + (monthNumber < 10 ? "0" + monthNumber : monthNumber) + separator + day;
}

function sendToTelegram (text: string, user_id: string, name: string) {
    bot.sendMessage(chatId, `${user_id}:${name}:  ${text}`);
}

function feedback(data: {user_id: string, text: string, email: string, name: string}, sqlClient: any) {
    const smtpConfig = {
        host: 'smtp.spaceweb.ru',
        port: 25,
        secure: false, // upgrade later with STARTTLS
        auth: {
            user: 'test@auckta.ru',
            pass: '1234567qwerty'
        }
    };
    let transporter = nodemailer.createTransport(smtpConfig);
    let message = {
        from: 'test@auckta.ru',
        to: 'snikius@mail.ru',
        envelope: {
            from: 'Sberbank <test@auckta.ru>',
            to: 'snikius@mail.ru',
        },
        subject: 'Обратная связь',
        html: '<table>' +
        '<tr><th>Имя</th><td>' + data.name + '</td></tr>' +
        '<tr><th>Email</th><td>' + data.email + '</td></tr>' +
        '<tr><th>Текст</th><td>' + data.text + '</td></tr>' +
        '</table>',
    };

    sendToTelegram(data.text, data.user_id, data.name);

    return new Promise((resolve, reject) => {
        // verify connection configuration
        transporter.verify(function(error, success) {
            if (error) {
                reject("Ошибка отправки сообщения: " + error);
            } else {
                transporter.sendMail(message, (err) => {
                    err ? reject(err) : resolve();
                });
            }
        });

    });
}

function getManagerRecords(filter: {office?: string, date?: string}, sqlClient: any): Promise<Array<any>> {
    let i = 1;
    const query = {
        text:
        'SELECT ' +
            'offices.name, ' +
            'offices.coords, ' +
            'offices.schedule, ' +
            'records.id, ' +
            'records.office_id, ' +
            'records.record, ' +
            'records.created, ' +
            'records.user_id, ' +
            'offices.address, ' +
            'users.name as user_name, ' +
            'users.phone, ' +
            '(records.record > NOW()) AS is_active ' +
        'FROM records ' +
        'INNER JOIN users ON users.user_id = records.user_id ' +
        'INNER JOIN offices ON offices.id = records.office_id ' +
        (filter.office ? `WHERE office_id = $${i++} ` : '') +
        (filter.date ? `${i == 1 ? 'WHERE' : 'AND'} to_char(record, 'YYYY/MM/DD') = $${i++} ` : '') +
        'ORDER BY records.record DESC ' +
        'LIMIT 100 ',
        values: [],
    };
    if (filter.office) {
        query.values.push(filter.office);
    }
    if (filter.date) {
        query.values.push(filter.date);
    }
    return sqlClient.query(query).then((res) => {
        return res.rows.length ? res.rows : [];
    });
}

function getManagerRecord(id: string, sqlClient: any): Promise<any> {
    const query = {
        text:
        'SELECT DISTINCT ON (records.id) ' +
            'offices.name, ' +
            'offices.coords, ' +
            'offices.schedule, ' +
            'records.id, ' +
            'records.office_id, ' +
            'records.record, ' +
            'records.created, ' +
            'records.user_id, ' +
            'records.is_visited, ' +
            'records.visit_reason, ' +
            'users.name as user_name, ' +
            'records.user_visit_time, ' +
            'offices.address, ' +
            '(records.record > NOW()) AS is_active ' +
            'FROM records ' +
        'INNER JOIN users ON users.user_id = records.user_id ' +
        'INNER JOIN offices ON offices.id = records.office_id ' +
        'WHERE records.id = $1 ',
        values: [id],
    };

    return sqlClient.query(query).then((res) => {
        return res.rows.length ? res.rows[0] : [];
    });
}

function saveManagerRecord(
        id: string,
        data: {visit_reason: string, user_visit_time: string, user_name: string, is_visited: boolean | null},
        sqlClient: any
    ): Promise<any> {
    const query = {
        text:
        'UPDATE records ' +
        'SET is_visited = $1, visit_reason = $2, user_name = $3, user_visit_time = $4 ' +
        'WHERE records.id = $5 ',
        values: [!!data.is_visited, data.visit_reason, data.user_name, data.user_visit_time, id],
    };

    return sqlClient.query(query);
}

module.exports = {addVersion, toPromiseWrapper, timeToStr, timeToDateStr, feedback, getManagerRecords, getManagerRecord, saveManagerRecord};