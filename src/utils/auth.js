// @flow
const crypto = require('crypto');

const {TUser} = require('./../models');

const PHONE_CONFIRM = 'phone_confirm_code';
const TOKENS = 'tokens';

function provideUser(phone: string, name: string, sqlClient: any): Promise<TUser> {
    const queryInsert = {
        text: 'INSERT INTO users(phone, name) VALUES ($1, $2)',
        values: [phone, name],
        rowMode: 'array',
    };
    const querySelect = {
        text: 'SELECT * FROM users WHERE phone = $1',
        values: [phone],
    };
    return new Promise((resolve, reject) => {
        sqlClient.query(querySelect).then((data) => {
            if (data.rows[0]) {
                resolve(data.rows[0]);
                return;
            }
            sqlClient.query(queryInsert).then((data) => {
                sqlClient.query(querySelect).then((data) => {
                    if (data.rows[0]) {
                        resolve(data.rows[0]);
                    } else {
                        reject("Error while user creating");
                    }
                });
            }).catch((error) => {
                reject(error.stack);
            });
        }).catch((error) => {
            reject(error.stack);
        });
    });
}

function generateToken (data: TUser, pClient: Function, sqlClient: any): Promise<{phone: string, token: string, user_id: string}> {
    const {phone} = data;
    return new Promise((resolve, reject) => {
        crypto.randomBytes(32, (err, buf) => {
            if (err) {
                reject({phone});
            } else {
                const token = buf.toString('hex');
                pClient('set')(`${TOKENS}:${token}`, phone, 'EX', 3600 * 730);
                provideUser(data.phone, data.name, sqlClient).then((data: TUser) => {
                    resolve({phone, token, user_id: data.user_id});
                }, reject);
            }
        });
    });
}

function checkAuth(token: string, pClient: Function, sqlClient: any): Promise<{phone: string, token: string, user_id: string}> {
    return new Promise((resolve, reject) => {
        pClient('get')(`${TOKENS}:${token}`).then((phone) => {
            if (phone) {
                provideUser(phone, '', sqlClient).then((data: TUser) => {
                    const {user_id, phone, name} = data;
                    resolve({token, phone, user_id, name});
                }, reject);
            } else {
                reject()
            }
        }, reject);
    });
}

function request(data: {phone: string}, client: any): {phone: string} {
    const code: string = '111111';
    client.set(`${PHONE_CONFIRM}:${data.phone}`, code, 'EX', 3600);
    if (data.phone === '+79111891118') {
        client.set(`${PHONE_CONFIRM}:${data.phone}`, '111111');
    }
    return Object.assign({}, data);
}

function checkCode(data: {phone: string, code: string, name: string}, pClient: Function, sqlClient: any): Promise<{phone: string, token: string, user_id: string}> {
    const {phone, code} = data;
    return new Promise((resolve, reject) => {
        pClient('get')(`${PHONE_CONFIRM}:${phone}`).then((keyData) => {
            if (keyData === code) {
                generateToken(data, pClient, sqlClient).then(resolve, reject);
            } else {
                reject({phone});
            }
        }, reject);
    });
}

function logout(token: string, pClient: Function): Promise<void> {
    return new Promise((resolve, reject) => {
        pClient('del')(`${TOKENS}:${token}`).then(() => {
           resolve();
        }, reject);
    });
}

module.exports = {request, checkCode, checkAuth, logout};