// @flow
const {TSchedule, TDaySchedule, TDay, TRecordPeriod} = require('./../models');
const {timeToStr, timeToDateStr} = require('./common');

function filterPeriods (schedule: TSchedule, date: Date, table: string[], forceBusy: boolean): Array<TRecordPeriod> {
    const dayKey: TDay = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'][date.getDay()];
    const scheduleTime: TDaySchedule = schedule.days[dayKey] ? schedule.days[dayKey] : null;
    if (!scheduleTime) {
        return [];
    }
    const start = +scheduleTime.start.split(':')[0];
    const end = +scheduleTime.end.split(':')[0];
    let hours: Array<TRecordPeriod> = [];
    for (let h = start; h < end; h++) {
        let copiedDate = new Date(date);
        let m = 0;
        copiedDate.setHours(h);
        copiedDate.setMinutes(0);
        copiedDate.setSeconds(0);
        copiedDate.setMilliseconds(0);

        let isPast = copiedDate < new Date();
        hours.push({h, m, time: timeToStr(copiedDate), busy: isPast || forceBusy || table.indexOf(timeToStr(copiedDate)) !== -1});
        m = 20;
        copiedDate.setMinutes(20);
        isPast = copiedDate < new Date();
        hours.push({h, m, time: timeToStr(copiedDate), busy: isPast || forceBusy || table.indexOf(timeToStr(copiedDate)) !== -1});
        m = 40;
        copiedDate.setMinutes(40);
        isPast = copiedDate < new Date();
        hours.push({h, m, time: timeToStr(copiedDate), busy: isPast || forceBusy || table.indexOf(timeToStr(copiedDate)) !== -1});
    }
    return hours;
}

function getRecordPeriods (date: Date, schedule: TSchedule, office_id: string, region: string, clientRedis: Function, sqlClient: any): Promise<Array<TRecordPeriod>> {
    const queryDate = {
        text: 'SELECT value FROM settings WHERE parameter = $1',
        values: ['date'],
    };
    let forceBusy = false;
    const dayStr = timeToDateStr(date);
    return new Promise((resolve, reject) => {
        sqlClient.query(queryDate).then((result) => {
            const dates = result.rows[0] ? result.rows[0].value : {};
            if (dates[region] && date.getTime() > new Date(dates[region])) {
                forceBusy = true;
            }
            clientRedis('hkeys')(`records:${office_id}:${dayStr}`).then((result) => {
                resolve(filterPeriods(schedule, date, result, forceBusy));
            }, reject);
        });
    });
}

module.exports = {getRecordPeriods};
