// @flow

const { Client } = require('pg');
const fs = require('fs');

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: '5573600s',
    port: 5432,
});

client.connect();

const offices = JSON.parse(fs.readFileSync('json/data.json', 'utf8'));

type TOfficeSource = {
    name: string,
    id: string,
    phone: string,
    address: string,
    description: string,
    schedule: any,
    coordinates: {latitude: number, longitude: number}
};

client.query("DELETE FROM offices", (err, res) => {
    if (err) {
        console.log(err.stack)
    }
});


function handleOffice (office: TOfficeSource) {
    const query = {
        text: 'INSERT INTO offices VALUES ($1, $2, point($3, $4), $5, $6, $7)',
        values: [office.id, office.name, office.coordinates.latitude, office.coordinates.longitude, JSON.stringify(office.schedule), "Описание офиса", office.address],
        rowMode: 'array',
    };
    client.query(query, (err, res) => {
        if (err) {
            console.log(err.stack)
        }
    });
}
offices.forEach(handleOffice);


/**
 CREATE TABLE public.offices
 (
 id bigint NOT NULL,
 name character varying(40) NOT NULL,
 coords point NOT NULL,
 shedule json NOT NULL,
 description character varying(160),
 address character varying(160),
 CONSTRAINT offices_pkey PRIMARY KEY (id)
 )
 */


