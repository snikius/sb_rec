

export type TOffice = {
    name: string,
    id: string,
    phone: string,
    address: string,
    description: string,
    schedule: any,
    coordinates: {latitude: number, longitude: number}
};

export type TDay = 'sunday' | 'monday' | 'tuesday' | 'wednesday' | 'thursday' | 'friday' | 'saturday';

export type TDaySchedule = {
    start: string,
    end: string,
};

export type TSchedule = {
    days: {
        monday?: TDaySchedule,
        tuesday?: TDaySchedule,
        wednesday?: TDaySchedule,
        thursday?: TDaySchedule,
        friday?: TDaySchedule,
        saturday?: TDaySchedule,
        sunday?:TDaySchedule,
    }
};

export type TRecord = {
    id?: string,
    created?: string,
    record?: string,
    user_id?: string,
    user_name?: string,
    office_id?: string,
    is_active?: boolean;
};

export type TRecordPeriod = {
    h: string,
    m: string,
    time: string,
    busy: boolean,
};

export type TUser = {
    user_id?: string,
    phone?: string,
    name?: string,
};
