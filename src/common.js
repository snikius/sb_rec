// @flow

const express = require('express');
const bodyParser = require('body-parser');
const {Client} = require('pg');
const Moment = require('moment');
const {isEmpty} = require('lodash');

const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'prod';
const isDev = env !== 'prod';
const config = require('./../config')[env];

const common = require('./utils/common');

const engines = require('consolidate');

const sqlClient = new Client(config.connect);
sqlClient.connect();

const app = express();

const redis = require("redis");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(express.static('static/files'));

app.engine('html', engines.mustache);
app.set('view engine', 'html');
app.set('views', __dirname + '/../static');

app.post('/api/feedback', function (req, res) {
    if (!req.body.user_id || !req.body.text) {
        res.status(400).send({
            result: 0,
            msg: "Нет ID"
        });
        return;
    }
    common.feedback(req.body, sqlClient).then((data) => {
        res.send({
            result: 1,
            data
        });
    }).catch((error) => {
        res.send({
            result: 0,
            error
        });
    });
});

app.get('/static/help', function (req, res) {
    res.render('help.html', {});
});

app.get('/static/info', function (req, res) {
    res.render('info.html', {});
});

app.get('/manager/view', function (req, res) {
    const dateParam: string = req.query.date ? req.query.date : common.timeToDateStr(new Date(), '/');
    const office: string  = req.query.office ? req.query.office : '';

    const date = dateParam;

    common.getManagerRecords({date, office}, sqlClient).then((data) => {
        res.render('manager.html', {
            data: data.map((row) => {
                row.record = Moment(row.record).format("HH:mm");
                row.created = Moment(row.created).format("DD.MM.YY HH:mm");
                return row;
            }),
            date,
            office,
        });
    });
});

app.get('/manager/visit/:id', function (req, res) {
    const id: string = req.params.id;

    common.getManagerRecord(id, sqlClient).then((data) => {
        const {record, is_visited, visit_reason, id, address, user_visit_time, user_name} = data;
        const date = Moment(record).format("DD.MM.YY HH:mm");


        const common = [
            {value: 'Наличные, пенсии'},
            {value: 'Платежи, квартплата, телефон, ГИБДД'},
            {value: 'Справки, выписки, доверенности'},
            {value: 'Карты, забрать готовую, перевыпустить'},
            {value: 'Вклады'},
            {value: 'Переводы'},
            {value: 'Кредит/Ипотека, ежемесячный платеж'},
            {value: 'Кредит оформить'},
            {value: 'Карты открыть новую'},
            {value: 'Переводы'},
            {value: 'Сейфы'},
            {value: 'Выплаты АСВ'},
            {value: 'Услуги для бизнеса'},
            {value: 'Монеты, Обмен валюты'},
            {value: 'Решить проблему'},
            {value: 'Ипотека оформить'},
        ];

        const mapper = (i) => {
            return {
                value: i.value,
                selected: visit_reason === i.value,
            };
        };

        res.render('visit.html', {
            record_id: id,
            record,
            date,
            address,
            is_visited,
            visit_reason,
            user_name,
            user_visit_time,
            common: common.map(mapper),
        });
    });
});

app.post('/manager/visit/:id', function (req, res) {
    const id: string = req.params.id;
    if (!isEmpty(req.body)) {
        common.saveManagerRecord(id, req.body, sqlClient).then(() => {
            res.redirect('/manager/visit/' + id);
        });
    } else {
        res.redirect('/manager/visit/' + id);
    }
});


const port = isDev ? 4005 : 3005;
app.listen(port, function () {
    console.log('Common api endpoint listening on port ' + port);
});