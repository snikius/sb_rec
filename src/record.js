// @flow

const express = require('express');
const bodyParser = require('body-parser');
const { Client } = require('pg');

const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'prod';
const isDev = env !== 'prod';
const config = require('./../config')[env];

const {createRecord, getRecords, deleteRecord} = require('./utils/records');

const redis = require("redis");
const clientRedis = redis.createClient();

const sqlClient = new Client(config.connect);

sqlClient.connect();

const app = express();

app.use(bodyParser.json());

app.post('/api/record', function (req, res) {
    createRecord(req.body, sqlClient, clientRedis).then((data) => {
        res.send({
            result: 1,
            data
        });
    }).catch((error) => {
        res.send({
            result: 0,
            msg: error.stack ? error.stack : error,
        });
    });
});

app.get('/api/records', function (req, res) {
    if (!req.query.user_id) {
        res.status(400).send({
            result: 0,
            msg: "Нет ID"
        });
    } else {
        getRecords(req.query.user_id, sqlClient).then(function (data) {
            res.send({
                result: 1,
                data
            });
        }).catch((error) => {
            res.send({
                result: 0,
                msg: error.stack
            });
        });
    }
});

app.delete('/api/record', function (req, res) {
    if (!req.query.user_id && !req.query.record_id) {
        res.status(400).send({
            result: 0,
            msg: "Нет ID"
        });
    } else {
        const {user_id, record_id} = req.query;
        deleteRecord(user_id, record_id, sqlClient, clientRedis).then(function (data) {
            res.send({
                result: 1,
                data
            });
        }, (error) => {
            res.status(400).send({
                result: 0,
                msg: error.stack
            });
        });
    }
});

const port = isDev ? 4004 : 3004;
app.listen(port, function () {
    console.log('Records endpoint listening on port ' + port);
});