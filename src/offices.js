// @flow

const express = require('express');
const bodyParser = require('body-parser');
const { Client } = require('pg');

const {getRecordPeriods} = require('./utils/offices');

const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'prod';
const isDev = env !== 'prod';
const config = require('./../config')[env];

const auth = require('./utils/auth');
const common = require('./utils/common');

const sqlClient = new Client(config.connect);
sqlClient.connect();

const app = express();

const redis = require("redis");
const clientRedis = redis.createClient();
const clientRedisPromised = common.toPromiseWrapper(clientRedis);

app.use(bodyParser.json());
app.use(express.static('static/files'));

let regions = [];
const queryRegions = {
    text: 'SELECT value FROM settings WHERE parameter = $1',
    values: ['regions'],
};

app.get('/api/offices', function (req, res) {
    if (!req.query.latitude || !req.query.longitude) {
        res.status(400).send({
            result: 0,
            msg: "Нет кординат"
        });
        return;
    }
    sqlClient.query(queryRegions).then((result) => {
        const regions = result.rows[0] ? result.rows[0].value : [];
        const query = {
            text: '' +
            'SELECT id, name, description,  reasons, coords, address, schedule ' +
            'FROM offices ' +
            'WHERE region = ANY($3::int[]) AND coords <-> point($1, $2) < 2',
            values: [req.query.latitude, req.query.longitude, regions],
        };

        sqlClient.query(query, (err, result) => {
            if (err) {
                console.log(err.stack);
                res.status(500).send({result: 0, data: [], error: err.stack});
            } else {
                res.send({result: 1, data: result.rows.map((row) => {
                    row.coordinates = {latitude: row.coords.x, longitude: row.coords.y};
                    delete row.coords;
                    return row;
                })});
            }
        });
    });
});

app.get('/api/office/:id/date/:date(\\d{4}-\\d{2}-\\d{2})', function (req, res) {
    const query = {
        text: 'SELECT id, name, description, reasons, coords, schedule, region FROM offices WHERE id = $1',
        values: [req.params.id],
    };
    sqlClient.query(query, (err, result) => {
        if (err) {
            console.log(err.stack);
            res.status(500).send({result: 0, data: [], error: err.stack});
        } else if (!result.rows.length) {
            res.status(404).send({result: 0, data: {}});
        } else {
            let data = result.rows[0];
            getRecordPeriods(new Date(req.params.date + "T12:00:00.000Z"), data.schedule, req.params.id, data.region, clientRedisPromised, sqlClient).then((periods) => {
                data.periods = periods;
                res.status(200).send({result: 1, data});
            });
        }
    });
});

const port = isDev ? 4003 : 3003;
app.listen(port, function () {
    console.log('Offices endpoint listening on port ' + port);
});