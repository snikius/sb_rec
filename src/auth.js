// @flow
const express = require('express');
const bodyParser = require('body-parser');
const { Client } = require('pg');

const auth = require('./utils/auth');
const common = require('./utils/common');

const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'prod';
const isDev = env !== 'prod';
const config = require('./../config')[env];

let redis = require("redis");

let client = redis.createClient();
let clientPromised = common.toPromiseWrapper(client);

const sqlClient = new Client(config.connect);
sqlClient.connect();

let app = express();

const version = '0.01';

app.use(bodyParser.json());
app.use(express.static('static/files'));

type RequestResponse = {
    result: number,
    [string]: any,
};

function filterData(data: any): any {
    if (data.phone) {
        data.phone = data.phone.replace(/\s/, '');
    }
    return data;
}

app.post('/api/user/request', function (req, res) {
    let response: RequestResponse  = {result: 0};
    if (!req.body.phone) {
        response = {
            result: 0,
            msg: "Телефон не указан"
        };
    } else {
        req.body = filterData(req.body);
        const data = common.addVersion(auth.request(req.body, client), version);
        response = Object.assign(data, {result: 1});
    }
    res.send(response);
});

app.post('/api/user/check', function (req, res) {
    if (!req.body.phone) {
        res.send({
            result: 0,
            msg: "Телефон не указан"
        });
    } else if (!req.body.code) {
        res.send({
            result: 0,
            msg: "Проверочный код не указан"
        });
    } else {
        req.body = filterData(req.body);
        auth.checkCode(req.body, clientPromised, sqlClient).then((data) => {
            res.send({
                result: 1,
                data
            });
        }, () => {
            res.send({
                result: 0,
                msg: "Неверный код"
            });
        });
    }
});

app.post('/api/user/token', function (req, res) {
    if (!req.body.token) {
        res.send({
            result: 0,
            msg: "Нет токена"
        });
    } else {
        auth.checkAuth(req.body.token, clientPromised, sqlClient).then((data) => {
            res.send({
                result: 1,
                data,
            });
        }, () => {
            res.send({
                result: 0,
                msg: "Неверный токен"
            });
        });
    }
});

app.post('/api/user/logout', function (req, res) {
    if (!req.body.token) {
        res.send({
            result: 0,
            msg: "Нет токена"
        });
    } else {
        auth.logout(req.body.token, clientPromised).then(() => {
            res.send({
                result: 1,
            });
        }, () => {
            res.send({
                result: 0,
                msg: "Ошибка"
            });
        });
    }
});

const port = isDev ? 4002 : 3002;
app.listen(port, function () {
    console.log('Auth endpoint listening on port ' + port);
});