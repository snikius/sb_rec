module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    {
        name      : 'Auth API Production',
        script    : './build/auth.js',
        watch: false,
        env: {
          "NODE_ENV": "prod",
        }
    },
    {
        name      : 'Offices API Production',
        script    : './build/offices.js',
        watch: false,
        env: {
            "NODE_ENV": "prod",
        }
    },
    {
        name      : 'Record API Production',
        script    : './build/record.js',
        watch: false,
        env: {
            "NODE_ENV": "prod",
        }
    },
    {
        name      : 'Common API Production',
        script    : './build/common.js',
        watch: false,
        env: {
            "NODE_ENV": "prod",
        }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : 'root',
      host : '179.60.149.123',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:snikius/sb_rec.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js'
    },
  }
};
