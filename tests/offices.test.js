const request = require('request');

const {Client} = require('pg');
const config = require('./../config')['testing'];
const sqlClient = new Client(config.connect);
sqlClient.connect();

let id = null;

beforeAll(() => {
    return setActiveSetting();
});

function setActiveSetting() {
    return new Promise(function(resolve, reject) {
        const query1 = {
            text: 'DELETE FROM settings WHERE parameter = $1',
            values: ['regions'],
        };
        const query2 = {
            text: 'INSERT INTO settings (parameter, value) VALUES ($1, $2)',
            values: ['regions', JSON.stringify([77, 78])],
        };
        sqlClient.query(query1, (err, result) => {
            sqlClient.query(query2, (err, result) => {
                resolve();
            });
        });
    });
}

function setDisableSetting() {
    return new Promise(function(resolve, reject) {
        const query1 = {
            text: 'DELETE FROM settings WHERE parameter = $1',
            values: ['regions'],
        };
        const query2 = {
            text: 'INSERT INTO settings (parameter, value) VALUES ($1, $2)',
            values: ['regions', JSON.stringify([78])],
        };
        sqlClient.query(query1, (err, result) => {
            sqlClient.query(query2, (err, result) => {
                resolve();
            });
        });
    });
}

test('Получение списка отделений', done => {
    expect.assertions(7);
    request.get('http://localhost:4003/api/offices?latitude=55.7&longitude=37.5', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(json.data.length).toBeGreaterThan(0);
        expect(!!json.data[0].id).toBe(true);
        expect(!!json.data[0].schedule).toBe(true);
        expect(!!json.data[0].coordinates.latitude).toBe(true);
        expect(!!json.data[0].coordinates.longitude).toBe(true);
        expect(!!json.data[0].address).toBe(true);
        id = json.data[0].id;
        done();
    });
});

test('Получение данных отделения', done => {
    expect.assertions(9);
    request.get('http://localhost:4003/api/office/' + id + '/date/2017-02-09', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(!!json.data.id).toBe(true);
        expect(!!json.data.reasons).toBe(true);
        expect(!!json.data.periods).toBe(true);
        expect(!!json.data.periods.length).toBe(true);
        const period = json.data.periods[0];
        expect(Number.isInteger(period.h)).toBe(true);
        expect(Number.isInteger(period.m)).toBe(true);
        expect(period.busy).toBe(false);
        expect(!!period.time).toBe(true);

        done();
    });
});

test('Получение списка отделений (проверка настроек, все доступны)', done => {
    expect.assertions(2);
    request.get('http://localhost:4003/api/offices?latitude=55.7&longitude=37.5', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(json.data.length).toBe(3);
        done();
    });
});


test('Получение списка отделений (проверка настроек, только 1 доступен)', done => {
    expect.assertions(3);
    setDisableSetting().then(() => {
        request.get('http://localhost:4003/api/offices?latitude=55.7&longitude=37.5', {form:{}}, (error, httpResponse, body) => {
            const json = JSON.parse(body);
            expect(json.result).toBe(1);
            expect(json.data.length).toBe(1);
            expect(json.data[0].id).toBe("900");
            done();
        });
    });
});
