const request = require('request');

let user_id = 1;

test('Отправляем обратную связь', done => {
    expect.assertions(1);
    const data = {
        text: 'Test',
        user_id,
    };
    request.post('http://localhost:4005/api/feedback', {json: data}, (error, httpResponse, json) => {
        expect(1).toBe(1);
        done();
    });
}, 10000);