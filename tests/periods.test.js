const request = require('request');
const {Client} = require('pg');

let id = "389038014";
let time = null;
let record_id = null;
let user_id = 1;

const config = require('./../config')['testing'];
const sqlClient = new Client(config.connect);
sqlClient.connect();

function setActiveSetting() {
    return new Promise(function(resolve, reject) {
        const query1 = {
            text: 'DELETE FROM settings WHERE parameter = $1',
            values: ['date'],
        };
        const query2 = {
            text: 'INSERT INTO settings (parameter, value) VALUES ($1, $2)',
            values: ['date', JSON.stringify({
                77: '2023-03-09',
                78: '2023-03-09',
            })],
        };
        sqlClient.query(query1, (err, result) => {
            sqlClient.query(query2, (err, result) => {
                resolve();
            });
        });
    });
}

beforeAll(() => {
    setActiveSetting();
    return new Promise((resolve, reject) => {
        request.get('http://localhost:4004/api/records?user_id=' + user_id, {form:{}}, (error, httpResponse, body) => {
            const json = JSON.parse(body);
            const records = json.data;
            if (records.length) {
                Promise.all(records.map((rec) => {
                    return request.delete(`http://localhost:3004/api/record?user_id=${user_id}&record_id=${rec.id}`);
                })).then(() => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    });
});

test('Получение данных отделения (прошлое)', done => {
    expect.assertions(7);
    request.get('http://localhost:4003/api/office/' + id + '/date/2017-02-09', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(!!json.data.periods).toBe(true);
        expect(!!json.data.periods.length).toBe(true);
        const period = json.data.periods[0];
        expect(Number.isInteger(period.h)).toBe(true);
        expect(Number.isInteger(period.m)).toBe(true);
        expect(period.busy).toBe(true);
        expect(!!period.time).toBe(true);

        time = period.time;
        done();
    });
});

test('Получение данных отделения (будущее)', done => {
    expect.assertions(7);
    request.get('http://localhost:4003/api/office/' + id + '/date/2021-02-11', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(!!json.data.periods).toBe(true);
        expect(!!json.data.periods.length).toBe(true);
        const period = json.data.periods[0];
        expect(Number.isInteger(period.h)).toBe(true);
        expect(Number.isInteger(period.m)).toBe(true);
        expect(period.busy).toBe(false);
        expect(!!period.time).toBe(true);

        time = period.time;
        done();
    });
});

test('Запись на выбранное время', done => {
    expect.assertions(5);
    const recordData = {
        record: time,
        user_id,
        office_id: id,
    };
    request.post({url: 'http://localhost:4004/api/record', json: recordData}, (error, httpResponse, json) => {
        expect(json.result).toBe(1);
        expect(!!json.data.created).toBe(true);
        expect(!!json.data.id).toBe(true);
        expect(json.data.user_id).toBe("1");
        expect(json.data.office_id).toBe(id);

        record_id = json.data.id;
        done();
    });
});

test('Получение данных отделения с занятым периодом, после записи', done => {
    expect.assertions(7);
    request.get('http://localhost:4003/api/office/' + id + '/date/2021-02-11', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(!!json.data.periods).toBe(true);
        expect(!!json.data.periods.length).toBe(true);
        const period = json.data.periods[0];
        expect(Number.isInteger(period.h)).toBe(true);
        expect(Number.isInteger(period.m)).toBe(true);
        expect(period.busy).toBe(true);
        expect(!!period.time).toBe(true);
        done();
    });
});

test('Удаление записи', done => {
    expect.assertions(1);

    request.delete(`http://localhost:4004/api/record?user_id=${user_id}&record_id=${record_id}`, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        done();
    });
});

test('Получение данных отделения с свободным периодом, после удаления записи', done => {
    expect.assertions(7);
    setTimeout(() => {
        request.get('http://localhost:4003/api/office/' + id + '/date/2021-02-11', {form:{}}, (error, httpResponse, body) => {
            const json = JSON.parse(body);
            expect(json.result).toBe(1);
            expect(!!json.data.periods).toBe(true);
            expect(!!json.data.periods.length).toBe(true);
            const period = json.data.periods[0];
            expect(Number.isInteger(period.h)).toBe(true);
            expect(Number.isInteger(period.m)).toBe(true);
            expect(period.busy).toBe(false);
            expect(!!period.time).toBe(true);
            done();
        });
    }, 200);
});

test('Получение данных отделения с свободным периодом (проверка настроек, свободны)', done => {
    expect.assertions(4);
    request.get('http://localhost:4003/api/office/900/date/2021-02-11', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(!!json.data.periods).toBe(true);
        expect(json.data.periods.length).toBeGreaterThan(0);
        let allBusy = true;
        json.data.periods.forEach((period) => {
            !period.busy && (allBusy = false);
        });
        expect(allBusy).toBe(false);
        done();
    });
});

test('Получение данных отделения с свободным периодом (проверка настроек, после окончания)', done => {
    expect.assertions(4);
    request.get('http://localhost:4003/api/office/' + id + '/date/2024-02-14', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(!!json.data.periods).toBe(true);
        expect(json.data.periods.length).toBeGreaterThan(0);
        let allBusy = true;
        json.data.periods.forEach((period) => {
            !period.busy && (allBusy = false);
        });
        expect(allBusy).toBe(true);
        done();
    });
});