const request = require('request');

const {timeToStr} = require('./../src/utils/common');

let record_id = '';
const MILLISECS_PER_HOUR = 60  * 60  * 1000;
let currentDate = new Date();
let nextDate = new Date(currentDate.getTime() + 24 * MILLISECS_PER_HOUR);
let nextDateHour = new Date(currentDate.getTime() + 24 * MILLISECS_PER_HOUR + MILLISECS_PER_HOUR);
let user_id = 1;
let records = [];

beforeAll(() => {
    return new Promise((resolve, reject) => {
        request.get('http://localhost:4004/api/records?user_id=' + user_id, {form:{}}, (error, httpResponse, body) => {
            const json = JSON.parse(body);
            const records = json.data;
            if (records.length) {
                Promise.all(records.map((rec) => {
                    return request.delete(`http://localhost:4004/api/record?user_id=${user_id}&record_id=${rec.id}`);
                })).then(() => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    });
});

test('Запись в отделение', done => {
    expect.assertions(7);
    const recordData = {
        record: timeToStr(nextDate),
        user_id,
        office_id: 389038014,
        user_name: 'tester',
    };
    request.post({url: 'http://localhost:4004/api/record', json: recordData}, (error, httpResponse, json) => {
        if(error) {
            console.error(error);
        }
        expect(json.result).toBe(1);
        expect(!!json.data.created).toBe(true);
        expect(!!json.data.id).toBe(true);
        expect(json.data.user_id).toBe(user_id.toString());
        expect(json.data.user_name).toBe('tester');
        expect(json.data.office_id).toBe("389038014");
        expect(!!json.data.address).toBe(true);

        record_id = json.data.id;
        done();
    });
});

test('Получение списка записей', done => {
    expect.assertions(8);
    request.get('http://localhost:4004/api/records?user_id=' + user_id, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(!!json.data.length).toBe(true);
        const record = json.data[0];

        expect(!!record.created).toBe(true);
        expect(!!record.id).toBe(true);
        expect(record.user_id).toBe(user_id.toString());
        expect(record.office_id).toBe("389038014");
        expect(record.is_active).toBe(true);
        expect(!!record.address).toBe(true);

        done();

        records = json.data;
    });
});

test('Удаление записи', done => {
    expect.assertions(2);
    expect(records.length).toBeGreaterThan(0);
    request.delete(`http://localhost:4004/api/record?user_id=${user_id}&record_id=${records[0].id}`, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        done();
    });
    records.forEach((r) => {
        request.delete(`http://localhost:4004/api/record?user_id=${user_id}&record_id=${r.id}`);
    });
});

test('Проверка что запись удалена', done => {
    expect.assertions(2);
    request.get(`http://localhost:4004/api/records?user_id=${user_id}`, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(1);
        expect(!!json.data.length).toBe(false);
        done();
    });
});


test('Запись в отделение (попытка записаться на тот же день)', done => {
    expect.assertions(7);
    const recordData = {
        record: timeToStr(nextDate),
        user_id,
        office_id: 389038014,
    };
    const recordData2 = {
        record: timeToStr(nextDateHour),
        user_id,
        office_id: 389038014,
    };
    request.post({url: 'http://localhost:4004/api/record', json: recordData}, (error, httpResponse, json) => {
        if(error) {
            console.error(error);
        }

        expect(json.result).toBe(1);
        expect(!!json.data.created).toBe(true);
        expect(!!json.data.id).toBe(true);
        expect(json.data.user_id).toBe(user_id.toString());
        expect(json.data.office_id).toBe("389038014");
        expect(!!json.data.address).toBe(true);

        request.post({url: 'http://localhost:4004/api/record', json: recordData2}, (error, httpResponse, json) => {
            expect(json.result).toBe(0);
            done();
        });

    });
});
