const request = require('request');

const limit = 10000;

function runTest() {
    const promises = [];
    promises.push(request.post({url: 'http://77.247.243.183:3002/api/user/request', json: {phone: "+79111891118"}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        if(json.result) {
            request.post({url: 'http://77.247.243.183:3002/api/user/check', json: {phone: "+79111891118", code: '111111'}}, (error, httpResponse, body) => {
                return !!body.result;
            });
        } else {
            return false;
        }
    }));

    promises.push(request.get('http://77.247.243.183:3003/api/offices?latitude=55.7&longitude=37.' + (Math.random() * 10), {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        return json.result && !!json.data.length;
    }));

    promises.push(request.get('http://77.247.243.183:3003/api/office/38903801/date/2017-02-09', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        return json.result && !!json.data.periods;
    }));

    return Promise.all(promises);
}


test('Stress test', done => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 360000;

    expect.assertions(limit);
    let counter = 0;
    for (let i = 0; i < limit; i++) {
        setTimeout(function() {
            runTest().then((values) => {
                let result = values.reduce((before, value) => {
                    return before && !!value;
                }, true);
                expect(result).toBe(true);
                counter++;
                if (counter === limit) {
                    done();
                }
            });
        }, 5);
    }
});
