const request = require('request');

test('Пустой запрос на рест подтверждения телефона', done => {
    expect.assertions(1);
    request.post('http://localhost:4002/api/user/request', {form:{}}, (error, httpResponse, body) => {
        const json = JSON.parse(body);
        expect(json.result).toBe(0);
        done();
    });
});

test('Получение кода', done => {
    expect.assertions(2);

    request.post({url: 'http://localhost:4002/api/user/request', json: {phone: "+79111891118"}}, (error, httpResponse, body) => {
        expect(!error).toBe(true);
        expect(body.result).toBe(1);
        done();
    });
});

test('Проверка кода (нет кода)', done => {
    expect.assertions(2);

    request.post({url: 'http://localhost:4002/api/user/check', json: {phone: "+79111891118"}}, (error, httpResponse, body) => {
        expect(!error).toBe(true);
        expect(body.result).toBe(0);
        done();
    });
});

test('Проверка кода (неверный код)', done => {
    expect.assertions(2);

    request.post({url: 'http://localhost:4002/api/user/check', json: {phone: "+79111891118", code: '222222'}}, (error, httpResponse, body) => {
        expect(!error).toBe(true);
        expect(body.result).toBe(0);
        done();
    });
});

let token = '';
test('Проверка кода (верный код)', done => {
    expect.assertions(5);

    request.post({url: 'http://localhost:4002/api/user/check', json: {phone: "+79111891118", code: '111111'}}, (error, httpResponse, body) => {
        expect(!error).toBe(true);
        expect(body.result).toBe(1);
        expect(!!body.data.token).toBe(true);
        expect(!!body.data.user_id).toBe(true);
        expect(body.data.token.length).toBeGreaterThan(10);
        token = body.data.token;
        done();
    });
});

test('Проверка токена (неверный)', done => {
    expect.assertions(2);

    request.post({url: 'http://localhost:4002/api/user/token', json: {token: '123'}}, (error, httpResponse, body) => {
        expect(!error).toBe(true);
        expect(body.result).toBe(0);
        done();
    });
});

test('Проверка токена (верный)', done => {
    expect.assertions(4);

    request.post({url: 'http://localhost:4002/api/user/token', json: {token}}, (error, httpResponse, body) => {
        expect(!error).toBe(true);
        expect(body.result).toBe(1);
        expect(!!body.data).toBe(true);
        expect(!!body.data.user_id).toBe(true);
        done();
    });
});

test('Логаут', done => {
    expect.assertions(2);

    request.post({url: 'http://localhost:4002/api/user/logout', json: {token}}, (error, httpResponse, body) => {
        expect(!error).toBe(true);
        expect(body.result).toBe(1);
        done();
    });
});

test('Проверка токена после логаута', done => {
    expect.assertions(2);

    request.post({url: 'http://localhost:4002/api/user/token', json: {token}}, (error, httpResponse, body) => {
        expect(!error).toBe(true);
        expect(body.result).toBe(0);
        done();
    });
});